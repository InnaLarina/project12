﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking
{
    public class History
    {
        public int Id { get; set; }
        public DateTime OperDate { get; set; }
        public OperationType Operartion { get; set; }
        public decimal sum { get; set; }
        public int IdAccount { get; set; }
        public History(int Id, DateTime OperDate, OperationType Operartion, decimal sum, int IdAccount)
        {
            this.Id = Id;
            this.OperDate = OperDate;
            this.Operartion = Operartion;
            this.sum = sum;
            this.IdAccount = IdAccount;
        }
        public override string ToString()
        {
            return "Operation Id: " + Id + ", OperDate: " + OperDate.ToShortDateString() + ", Operation: " + OperationType.inflow.ToString() + ", sum: " + sum;
        }
    }
}
