﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public string Passport { get; set; }
        public DateTime DateReg { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public User(int Id, string Name, string LastName, string MiddleName, string Phone, string Passport, DateTime DateReg, string Login, string Password)
        {
            this.Id = Id;
            this.Name = Name;
            this.LastName = LastName;
            this.MiddleName = MiddleName;
            this.Phone = Phone;
            this.Passport = Passport;
            this.DateReg = DateReg;
            this.Login = Login;
            this.Password = Password;
        }

        public override string ToString()
        {
            return "Client: " + Id + " " + Name + " " + LastName + ", pasp:" + Passport + ", DateReg:" + DateReg.ToShortDateString() + ", Login:" + Login + ", Password:" + Password;
        }
    }
}
