﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Banking
{
    class Program
    {

        static void Main(string[] args)
        {
            BankingQueries bq = new BankingQueries();
            bq.listUser.Add(new User(1, "Alisson", "", "", "", "", DateTime.Now, "Alisson", "021092"));
            bq.listUser.Add(new User(4, "Virgil", "Van Dijk", "", "", "", DateTime.Now, "Virgil", "080791"));
            bq.listUser.Add(new User(32, "Joel", "Matip", "", "", "", DateTime.Now, "Joel", "080891"));
            bq.listUser.Add(new User(6, "Dejan", "Lovren", "", "", "", DateTime.Now, "Dejan", "050689"));
            bq.listUser.Add(new User(26, "Andrew Robertson", "", "", "", "", DateTime.Now, "Andrew", "110394"));
            bq.listUser.Add(new User(66, "Trent", "Alexandr-Arnold", "", "", "", DateTime.Now, "Trent", "071098"));
            bq.listUser.Add(new User(3, "Fabinho", "", "", "", "", DateTime.Now, "Fabinho", "231093"));

            bq.listAccount.Add(new Account(10, DateTime.Now, 40000, 1));
            bq.listAccount.Add(new Account(11, DateTime.Now, 80800, 1));
            bq.listAccount.Add(new Account(12, DateTime.Now, 500, 1));
            bq.listAccount.Add(new Account(40, DateTime.Now, 1140000, 4));
            bq.listAccount.Add(new Account(41, DateTime.Now, 51140, 4));
            bq.listAccount.Add(new Account(30, DateTime.Now, 511400, 3));
            bq.listAccount.Add(new Account(31, DateTime.Now, 88500, 3));

            bq.listHistory.Add(new History(10, DateTime.Now.AddDays(-89), OperationType.inflow, 45000, 10));
            bq.listHistory.Add(new History(11, DateTime.Now.AddDays(-59), OperationType.outflow, 1500, 10));
            bq.listHistory.Add(new History(12, DateTime.Now.AddDays(-39), OperationType.inflow, 101500, 10));
            bq.listHistory.Add(new History(20, DateTime.Now.AddDays(-49), OperationType.inflow, 35000, 11));
            bq.listHistory.Add(new History(21, DateTime.Now.AddDays(-39), OperationType.outflow, 500, 11));
            bq.listHistory.Add(new History(22, DateTime.Now.AddDays(-29), OperationType.inflow, 18500, 11));
            //1
            Console.WriteLine("This is an information about the user Trent Alexandr-Arnold:");
            User userAccount = bq.UserInfo("Trent", "071098");
            Console.WriteLine(userAccount);
            Console.WriteLine();
            //2
            Console.WriteLine("These are all the accounts of the user Alisson:");
            IEnumerable<Account> accountsForUser = bq.User_Accounts("Virgil", "Van Dijk");
            foreach (var au in accountsForUser)
            {
                Console.WriteLine(au.ToString());
            }
            Console.WriteLine();
            //3
            Console.WriteLine("These are the Alisson' accounts with theirs history:");
            IEnumerable<UserAccountHistory> historyUser = bq.UsersAccountsWithHistory(1);
            string old_account = "";
            String repeatedString = new String(' ', 45);
            Console.WriteLine("Alisson TheFootballPlayer account:");
            foreach (var hu in historyUser)
            {
                if (hu is UserAccountHistory)
                {
                    UserAccountHistory uah = hu as UserAccountHistory;
                    if (old_account != uah.accountString)
                        Console.WriteLine(uah.accountString + " " + uah.historyString);
                    else Console.WriteLine(repeatedString + " " + uah.historyString);
                    old_account = uah.accountString;
                }

            }
            Console.WriteLine();
            //4
            Console.WriteLine("These are inflow operations:");
            IEnumerable<UserAccountHistory> historyInflow = bq.UsersHistoryInflow();
            foreach (var hi in historyInflow)
            {
                Console.WriteLine(hi.historyString + " " + hi.userString);
            }
            Console.WriteLine();
            //5
            try
            {
                Console.Write("Input amount of income and press Enter, please: ");
                string sumString = Console.ReadLine();
                int sumAccount = Convert.ToInt32(sumString);
                Console.WriteLine($"These happy guys have more than {sumString} euro in theirs accounts.  ");
                IEnumerable<User> userAccounts = bq.UsersWithTotalMoreThan(sumAccount);
                foreach (User user in userAccounts)
                {
                    Console.WriteLine(user);
                }
                if (!userAccounts.Any()) Console.WriteLine("There is none");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Неверно введенные данные");
            }
        }
    }
}
