﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banking
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime DateOpen { get; set; }
        public decimal sum { get; set; }
        public int IdUser { get; set; }
        public Account(int Id, DateTime DateOpen, decimal sum, int IdUser)
        {
            this.Id = Id;
            this.DateOpen = DateOpen;
            this.sum = sum;
            this.IdUser = IdUser;
        }
        public override string ToString()
        {
            return "IdAccount: " + Id + ",DateOpen: " + DateOpen.ToShortDateString() + ", Sum:" + sum;
        }
    }
}
