﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Banking
{
    public class UserAccountHistory
    {
        public string userString;
        public string accountString;
        public string historyString;
        public UserAccountHistory(string userString, string accountString, string historyString/*, int account*/)
        {
            this.userString = userString;
            this.accountString = accountString;
            this.historyString = historyString;
        }
    }
    public class BankingQueries
    {
        public List<User> listUser;
        public List<Account> listAccount;
        public List<History> listHistory;
        public virtual User UserInfo(string pLogin, string pPassword)
        {
            User userAccount = listUser.Where(u => u.Login == pLogin && u.Password == pPassword).FirstOrDefault();
            return userAccount;
        }
        public BankingQueries()
        {
            listUser = new List<User>();
            listAccount = new List<Account>();
            listHistory = new List<History>();
        }
        public virtual IEnumerable<Account> User_Accounts(string pName, string pLastName)
        {
            IEnumerable<Account> accountsForUser = from account in listAccount
                                                   join user in listUser on account.IdUser equals user.Id
                                                   where user.Name == "pName" && user.LastName == pLastName
                                                   select account;
            return accountsForUser;
        }
        public virtual IEnumerable<UserAccountHistory> UsersAccountsWithHistory(int pId)
        {
            IEnumerable<UserAccountHistory> historyUser = from history in listHistory
                                                          join account in listAccount on history.IdAccount equals account.Id
                                                          join user in listUser on account.IdUser equals user.Id
                                                          where user.Id == pId
                                                          orderby account.Id, history.OperDate
                                                          select new UserAccountHistory(user.ToString(), account.ToString(), history.ToString());
            return historyUser;
        }
        public virtual IEnumerable<UserAccountHistory> UsersHistoryInflow()
        {
            IEnumerable<UserAccountHistory> historyInflow = from history in listHistory
                                                            join account in listAccount on history.IdAccount equals account.Id
                                                            join user in listUser on account.IdUser equals user.Id
                                                            where history.Operartion == OperationType.inflow
                                                            orderby history.OperDate
                                                            select new UserAccountHistory(user.ToString(), account.ToString(), history.ToString());
            return historyInflow;

        }
        public virtual IEnumerable<User> UsersWithTotalMoreThan(decimal ptotal)
        {
            IEnumerable<User> userAccounts = from user in listUser
                                             join account in listAccount on user.Id equals account.IdUser
                                             where account.sum > ptotal
                                             select user;
            return userAccounts;
        }
    }
}
